import PubmedApiClient from "./api/PubmedApiClient";

export default class ApplicationContext {
  constructor() {
    this.pubmedApiClient = null;
  }

  getPubmedApiClient() {
    if (this.pubmedApiClient === null) {
      this.pubmedApiClient = new PubmedApiClient();
    }
    return this.pubmedApiClient;
  }

  setPubmedApiClient(client) {
    this.pubmedApiClient = client;
  }
}
