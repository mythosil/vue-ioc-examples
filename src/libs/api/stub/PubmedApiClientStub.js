import PubmedApiResponse from "../PubmedApiResponse";

export default class PubmedApiClientStub {
  async search(term) {
    return new PubmedApiResponse(100, 0, 3, [111, 222, 333]);
  }
}
