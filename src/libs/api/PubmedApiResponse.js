export default class PubmedApiResponse {
  constructor(total = 0, start = 0, count = 0, ids = []) {
    this.total = total;
    this.start = start;
    this.count = count;
    this.ids = ids;
  }
}
