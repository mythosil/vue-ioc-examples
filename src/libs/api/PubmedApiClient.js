import axios from 'axios';
import PubmedApiResponse from './PubmedApiResponse';

export default class PubmedApiClient {
  async search(term) {
    const url = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi';
    const params = {
      db: 'pubmed',
      term: term
    };

    const res = await axios.get(url, { params: params });
    return this.convertXmlToResponseObj(res.data);
  }

  convertXmlToResponseObj(xml) {
    const parser = new DOMParser();
    const doc = parser.parseFromString(xml, "application/xml");

    const total = parseInt(doc.querySelector('Count').textContent);
    const start = parseInt(doc.querySelector('RetStart').textContent);
    const count = parseInt(doc.querySelector('RetMax').textContent);
    const ids = Array.from(doc.querySelectorAll('Id')).map(node => parseInt(node.textContent));

    return new PubmedApiResponse(total, start, count, ids);
  }
}
