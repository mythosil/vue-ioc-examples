import Vue from "vue";
import App from "./App.vue";
import ApplicationContext from "./libs/ApplicationContext";

Vue.config.productionTip = false;

Vue.prototype.$context = new ApplicationContext();

new Vue({
  render: h => h(App)
}).$mount("#app");
