import { mount } from "@vue/test-utils";
import PubmedSearchPage from "@/pages/PubmedSearchPage.vue";
import ApplicationContext from "@/libs/ApplicationContext";
import PubmedApiClientStub from "../../src/libs/api/stub/PubmedApiClientStub";
import flushPromises from "flush-promises";

describe("PubmedSearchPage.vue", () => {
  const context = new ApplicationContext();
  context.setPubmedApiClient(new PubmedApiClientStub());

  const wrapper = mount(PubmedSearchPage, {
    mocks: {
      $context: context
    }
  });

  it("renders api response", async () => {
    wrapper.find("form").trigger("submit.prevent");

    await flushPromises();

    expect(wrapper.html()).toContain("<p>Total: 100</p>");
    expect(wrapper.html()).toContain("<p>Start: 0</p>");
    expect(wrapper.html()).toContain("<p>Count: 3</p>");
    expect(wrapper.html()).toContain("<li>111</li>");
    expect(wrapper.html()).toContain("<li>222</li>");
    expect(wrapper.html()).toContain("<li>333</li>");
  });
});
